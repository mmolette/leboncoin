package com.molette.leboncoin.domain.mappers

interface BaseMapper<in R, out L> {
    fun mapToLocal(obj: R): L
}