package com.molette.leboncoin.domain.mappers

import com.molette.leboncoin.data.network.models.AlbumRemote
import com.molette.leboncoin.domain.models.Album

class AlbumMapper: BaseMapper<AlbumRemote, Album> {

    override fun mapToLocal(obj: AlbumRemote): Album {
        return Album(albumId = obj.albumId, id = obj.id, title = obj.title, url = obj.url, thumbnailUrl = obj.thumbnailUrl)
    }
}