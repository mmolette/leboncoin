package com.molette.leboncoin.domain.repositories

import com.molette.leboncoin.domain.models.Album
import kotlinx.coroutines.flow.Flow

interface AlbumRepository {

    val albums: Flow<List<Album>>

    suspend fun getAlbumsRemote()

    fun getAlbumsLocal(): Flow<List<Album>>
}