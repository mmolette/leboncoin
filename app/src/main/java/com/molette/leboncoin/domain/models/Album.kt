package com.molette.leboncoin.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "albums")
data class Album(
    val albumId: Long,
    @PrimaryKey
    val id: Long,
    val title: String,
    val url: String,
    val thumbnailUrl: String
) {
}