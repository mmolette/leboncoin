package com.molette.leboncoin.ui.main

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.molette.leboncoin.domain.models.Album
import com.molette.leboncoin.domain.repositories.AlbumRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val albumRepository: AlbumRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val albums: LiveData<List<Album>> = albumRepository.albums.asLiveData()

    init {
        getAlbumsRemote()
    }

    private fun getAlbumsRemote(){
        viewModelScope.launch(Dispatchers.Main) {
            albumRepository.getAlbumsRemote()
        }
    }
}