package com.molette.leboncoin.ui.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.webkit.WebSettings
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.molette.leboncoin.R
import com.molette.leboncoin.databinding.AlbumCellBinding
import com.molette.leboncoin.domain.models.Album

class AlbumAdapter(): RecyclerView.Adapter<AlbumViewHolder>() {

    var data: List<Album> = mutableListOf()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<AlbumCellBinding>(inflater, R.layout.album_cell, parent, false)

        return AlbumViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        val album = data[position]
        holder.bind(album)
    }
}

class AlbumViewHolder(val binding: AlbumCellBinding): RecyclerView.ViewHolder(binding.root){
    fun bind(album: Album){
        binding.album = album
        val glideUrl = GlideUrl(album.thumbnailUrl, LazyHeaders.Builder()
            .addHeader("User-Agent", WebSettings.getDefaultUserAgent(binding.root.context)).build())
        Glide.with(binding.root.context)
            .load(glideUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.albumThumbnail)
    }
}