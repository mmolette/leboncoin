package com.molette.leboncoin.data.repositories.implementation

import android.util.Log
import com.molette.leboncoin.data.database.AppDatabase
import com.molette.leboncoin.data.network.AlbumAPI
import com.molette.leboncoin.domain.mappers.AlbumMapper
import com.molette.leboncoin.domain.models.Album
import com.molette.leboncoin.domain.repositories.AlbumRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import retrofit2.await
import java.lang.Exception
import javax.inject.Inject

class AlbumRepositoryImpl @Inject constructor(val localSource: AppDatabase, val remoteSource: AlbumAPI): AlbumRepository {

    private val mapper = AlbumMapper()

    override val albums: Flow<List<Album>>
        get() = getAlbumsLocal()

    override suspend fun getAlbumsRemote() {
        withContext(Dispatchers.IO){
            try {
                val albumsRemote = remoteSource.getAlbums()
                val albums = albumsRemote.map { mapper.mapToLocal(it) }
                localSource.albumDao.insertAll(albums)
            }catch (e: Exception){
                Log.e("getAlbumsRemote", e.toString())
            }
        }
    }

    override fun getAlbumsLocal(): Flow<List<Album>> {
        return localSource.albumDao.getAll()
    }
}