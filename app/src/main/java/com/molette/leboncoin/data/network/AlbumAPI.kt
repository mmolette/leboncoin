package com.molette.leboncoin.data.network

import com.molette.leboncoin.data.network.models.AlbumRemote
import retrofit2.http.GET


interface AlbumAPI {

    @GET("img/shared/technical-test.json")
    suspend fun getAlbums(): List<AlbumRemote>
}