package com.molette.leboncoin.data.network.models

import kotlinx.serialization.Serializable

@Serializable
data class AlbumRemote(
    val albumId: Long,
    val id: Long,
    val title: String,
    val url: String,
    val thumbnailUrl: String
) {
}