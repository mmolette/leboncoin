package com.molette.leboncoin.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.molette.leboncoin.domain.models.Album
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

@Dao
interface AlbumDao: BaseDao<Album> {

    @Query("SELECT * FROM albums")
    fun getAll(): Flow<List<Album>>
}