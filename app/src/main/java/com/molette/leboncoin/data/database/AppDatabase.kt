package com.molette.leboncoin.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.molette.leboncoin.data.database.dao.AlbumDao
import com.molette.leboncoin.domain.models.Album

@Database(entities = [Album::class],
    version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract val albumDao: AlbumDao
}