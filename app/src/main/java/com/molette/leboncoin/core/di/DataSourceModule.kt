package com.molette.leboncoin.core.di

import android.content.Context
import androidx.room.Dao
import androidx.room.Room
import androidx.room.RoomDatabase
import com.molette.leboncoin.data.database.AppDatabase
import com.molette.leboncoin.data.database.dao.AlbumDao
import com.molette.leboncoin.data.network.AlbumAPI
import com.molette.leboncoin.data.repositories.implementation.AlbumRepositoryImpl
import com.molette.leboncoin.domain.repositories.AlbumRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DataSourceModule {

    @Provides
    @Singleton
    fun provideDb(@ApplicationContext context: Context): AppDatabase{
        return Room.databaseBuilder(context, AppDatabase::class.java, "albums_db").build()
    }

    @Provides
    @Singleton
    fun provideAlbumDao(db: AppDatabase): AlbumDao {
        return db.albumDao
    }
}