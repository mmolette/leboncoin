package com.molette.leboncoin.core.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.molette.leboncoin.BuildConfig
import com.molette.leboncoin.data.network.AlbumAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    private const val CONNECT_TIMEOUT = 10L
    private const val WRITE_TIMEOUT = 1L
    private const val READ_TIMEOUT = 20L
    private val baseUrl = BuildConfig.API_BASE_URL
    private val contentType = MediaType.get("application/json")

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient{
        return OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }

    @OptIn(kotlinx.serialization.UnstableDefault::class)
    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit{
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(
                Json(
                    configuration = JsonConfiguration(
                        encodeDefaults = false,
                        ignoreUnknownKeys = true,
                        useArrayPolymorphism = true
                    )
                ).asConverterFactory(contentType)
            )
            .build()
    }

    @Provides
    @Singleton
    fun provideAlbumService(retrofit: Retrofit): AlbumAPI {
        return retrofit.create(AlbumAPI::class.java)
    }
}
