package com.molette.leboncoin.core.di

import com.molette.leboncoin.data.database.AppDatabase
import com.molette.leboncoin.data.network.AlbumAPI
import com.molette.leboncoin.data.repositories.implementation.AlbumRepositoryImpl
import com.molette.leboncoin.domain.repositories.AlbumRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.android.scopes.FragmentScoped

@Module
@InstallIn(
    ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    @ActivityRetainedScoped
    fun provideAlbumRepository(db: AppDatabase, service: AlbumAPI): AlbumRepository {
        return AlbumRepositoryImpl(db, service)
    }
}