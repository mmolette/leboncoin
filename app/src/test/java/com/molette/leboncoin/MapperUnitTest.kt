package com.molette.leboncoin

import com.molette.leboncoin.data.network.models.AlbumRemote
import com.molette.leboncoin.domain.mappers.AlbumMapper
import org.hamcrest.core.IsEqual
import org.junit.Assert
import org.junit.Test

class MapperUnitTest {

    @Test
    fun albumMapping_isCorrect(){

        val remoteAlbum = AlbumRemote(
            id = 2L,
            albumId = 3L,
            thumbnailUrl = "Thumbnail test",
            url =  "Url test",
            title = "Title test"
        )
        val localAlbum = AlbumMapper().mapToLocal(remoteAlbum)

        Assert.assertThat(localAlbum.title, IsEqual(remoteAlbum.title))
        Assert.assertThat(localAlbum.url, IsEqual(remoteAlbum.url))
        Assert.assertThat(localAlbum.thumbnailUrl, IsEqual(remoteAlbum.thumbnailUrl))
        Assert.assertThat(localAlbum.albumId, IsEqual(remoteAlbum.albumId))
        Assert.assertThat(localAlbum.id, IsEqual(remoteAlbum.id))
    }
}