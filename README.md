# Leboncoin Albums

## Introduction

Application Android qui récupère des données depuis un web service et les affiche dans une liste.  
Compatible depuis Android API 19.  

### Fonctionnalités
* Récupération des données depuis le web service et parsing du JSON
* Données sauvegardées en cache dans une base de données Room
* Affichage de la liste des albums dans un recyclerview
* Support des changements de configuration
* Test unitaire pour le mapper

### Améliorations possibles
* Gestion des erreurs de l'API REST
* Ajouter des UseCase pour améliorer la séparation des responsabilités
* Gestion des cas erreurs et placeholder pour Glide
* Système de pagination pour la gestion des données

## Implémentation

Le projet est architecturé sur la base du pattern MVVM.    
Hilt est utiliśe pour l'injection de dépendances.  

### Data layer

J'utilise Retrofit en tant que client REST pour consommer les données depuis l'API et parser les données.  
Une fois que les données sont parsées, elles sont stockées dans une base de données Room afin de fournir un mode hors ligne. Les requêtes sont éxécutées dans la classe ReviewDao.  
La récupération des albums est effectué via une requête obersvable implémenté avec Flow. J'utilise Coroutines pour la gestion des threads.  

### Domain layer

Ce serait bien d'ajouter des UseCases pour améliorer la séparation des responsabilités même si cela ajoute beaucoup de boilerplate.  
J'utilise un mapper pour convertir l'object distant (API) en un object local. J'ai écris un test unitaire pour ce mapper afin d'éviter des régressions si le code change.    

### Presentation layer
  
Le ViewModel contient les données dans un object LiveData afin respecter le cycle de vie des vues.  
Le ViewModel est aussi utilisé pour gérer les changements de configurations.  
L'activity et le fragment observent les propriétés du ViewModel afin de binder les données à la vue. 
Glide est utilisé pour la récupération et la mise en cache des images.  

## Dépendances
* Retrofit
* Room
* Coroutines
* Hilt
* ViewModel
* Lifecycle
* Glide
